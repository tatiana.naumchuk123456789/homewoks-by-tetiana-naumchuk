
function tabs() {
        let tabTitles = document.querySelectorAll('.tabs-title')
	let tabsContent = document.querySelectorAll('.content')
    
        tabTitles.forEach(item => {
                item.addEventListener('click', selectTabTitle)
		})
    
        function selectTabTitle() {
        tabTitles.forEach(item => {
                item.classList.remove('active')
                })
        this.classList.add('active')
        let activeTabTitle = this.dataset.title
        selectTabContent(activeTabTitle)
        }

        function selectTabContent(activeTabTitle) {
	tabsContent.forEach(item => {
		if (item.classList.contains(activeTabTitle)) {
                item.classList.add('active-content')
                } else {
		item.classList.remove('active-content')
                }
	    })
	}
}

tabs()
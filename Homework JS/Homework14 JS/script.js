


$(document).ready(function() {
    $("a[href^='#']").click(function () {
        const event = $(this).attr('href')
        $('html, body').animate( {
            scrollTop: $(event).offset().top
        }, 2000)
    })

    $('.arrow-up').on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        }, 2000)
    })

    $(document).on('scroll', function () {
        if (window.innerHeight < window.scrollY) {
            $('.arrow-up').show()
        } else {
            $('.arrow-up').hide()
        }
    })

    $('.hide-show-btn').click(function() {
        $('.popular-posts').slideToggle()
    })
})




// Почему для работы с input не рекомендуется использовать события клавиатуры?
// События клавиатуры предназначены именно для работы с клавиатурой. Их можно использовать для проверки ввода в input, но будут баги. Также некоторые мобильные устройства не генерируют keypress/keydown, а сразу вставляют текст в поле. Обработать ввод на них при помощи клавиатурных событий нельзя.


const btns = document.querySelectorAll('.btn')

window.addEventListener('keydown', function (event) {

	btns.forEach(btn => {
		const key = btn.textContent
		if (
			event.code.toUpperCase() === `KEY${key}`.toUpperCase() ||
			event.key.toUpperCase() === key.toUpperCase()
		) {
			btn.classList.add('active')
		} else {
			btn.classList.remove('active')
		}
	})
})
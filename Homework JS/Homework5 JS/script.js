// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования.
// В JS и других языках програмирования есть множество специальных символов таких как: '[]','?','|' и т.д. Для того, чтобы что-то сделать с этими  символами (например найти буквально '[]'), нужно поставить обратную косую черту '/[]' непосредственно перед символом . В противном случае, они будут распознаваться JS как часть кода, а не как текст. Например, если в одинарных кавычках будет слово с апострофом, то сам апостроф JS будет распознавать как закрывающуюся часть кавычек. И потому, чтобы избежать таких багов использують "экранирование символов".

let firstName = prompt('Enter the first name')
let lastName = prompt('Enter the last name')
let birthDay = prompt('Enter your birthday', 'DD.MM.YYYY')
function createNewUser() {
	let newUser = {
		firstName: firstName,
		lastName: lastName,
		birthDay: birthDay,
		getLogin: function () {
			return firstName[0].toLowerCase() + lastName.toLowerCase()
		},
		getPassword: function () {
			return firstName[0].toUpperCase() + lastName.toLowerCase() + birthDay.slice(6)
		},
		// getAge: function () {
		// 	let currentYear = new Date().getFullYear()
		// 	let birthYear = parseInt(this.birthDay.slice(6))
		// 	let age = currentYear - birthYear
		// 	return age
		// },
		getAge: function() {
		let currentDate = new Date()
		let userBirthday = Date.parse(
			`${this.birthDay.slice(6)}-${this.birthDay.slice(3, 5)}-${this.birthDay.slice(0, 2)}`,
		)
		let age = ((currentDate - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0)
		if (age < currentDate) {
			return `You have got ${age - 1} years`
		} else {
			return `You have got ${age} years`
		}
	},

	}
	return newUser
}
console.log(createNewUser())
console.log(createNewUser().getPassword())
console.log(createNewUser().getAge())










// Опишите своими словами как работает цикл forEach.
// Цикл forEach используется для перебора массива. Выполняет указанную функцию один раз для каждого  его элемента. Функция принимает от 1 до 3 аргументов: item - очередной элемент массива, index - его номер, array - массив, который перебирается.


function filterBy(array, type) {
	return array.reduce((result, currentItem) => {
		if (typeof currentItem !== type) {
			result.push(currentItem)
		}
		return result
	}, [])
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'))
console.log(filterBy(['hello', 'world', 23, '23', null], 'number'))
console.log(filterBy(['hello', 'world', 23, '23', null], 'object'))







// Когда пользователь физически что-делает на странице (клацает мышкой, нажимает какую-то клавишу) происходит некое событие. Это своеобразный  сигнал от браузера о том, что что-то произошло. Обработчик событий - это функция, которая сработает, как только событие произошло. Тоесть она пытается понять, что именно произошло и что с этим дальше делать.


const price = document.createElement('div')
const input = document.createElement('input')
const label = document.createElement('label')
const p = document.createElement('p')
const span = document.createElement('span')
const button = document.createElement('button')

price.className = 'price-wrapper'
input.className = 'input-field'
label.className = 'text-label'
p.className = 'uncorrect-price'
span.className = 'curent-price'
button.className = 'button'

document.body.prepend(price)
label.innerText = 'Price, $'
label.style.color = 'white'

input.id = 'price'
label.for = 'price'

price.prepend(label, input)

input.type = 'number'

input.addEventListener('focus', function(){
    this.style.border = '5px solid green'
})
input.addEventListener('blur', function(){
	this.style.border = '1px solid black'
    console.log(input.value)
  
if (input.value < 0 || input.value === '') {
	this.style.border = '5px solid red'
	p.innerText = 'Enter correct price, please'
	price.append(p)
	input.style.color = 'black'
	p.style.display = 'block'
	
} else {
	p.style.display = 'none'
	span.innerText = `Current price: ${input.value}`
    input.style.color = 'green'
	price.prepend(span)
	button.innerText = 'X'
	price.prepend(button)

	button.addEventListener('click', function () {
		this.remove()
		span.remove()
		input.value = ''
	})
} 
})





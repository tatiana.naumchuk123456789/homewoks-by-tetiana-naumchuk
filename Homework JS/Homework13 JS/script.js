


const btn = document.querySelector('.change-theme')
const local = localStorage.getItem('theme')

btn.onclick = function () {
	if (localStorage.getItem('theme') === 'white') {
		localStorage.setItem('theme', 'orange')
		document.body.style.background = 'orange'
	} else {
		localStorage.setItem('theme', 'white')
		document.body.style.background = 'url(./img/background.png)'
	}
}

if (local && local === 'orange') {
	document.body.style.background = 'orange'
}

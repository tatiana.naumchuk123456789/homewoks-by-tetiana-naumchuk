
//setTimeout() дозволяє викликати функцію одноразово через певний проміжок часу. setInterval дозволяє викликати функцію регулярно, повторюючи виклик через певний проміжок часу.

//Задання нульової затримки в setTimeout() передбачає виконання фунції якнайшвидше після виконання коду. Вона спрацює миттєво лише тоді, коли є менше 5 вкладених таймерів. У протилежному випадку браузер закладає мінімальну затримку - 4мс.  Нульову затримку встанолювати не рекомендується, бо це може викликати помилку у деяких браузерах або некоректну роботу.  

//Тому що цикл буде й надалі працювати, тим самим буде перевантажувати процесор комп'ютера, що в подальшому може призвести до збоїв в роботі.



const images = document.querySelectorAll('.image-to-show')
const firstImage = images[0]
const lastImage = images[images.length - 1]
const stopButton = document.querySelector('.stop')
const startButton = document.querySelector('.start')

const changeImg = () => {
	const currentImage = document.querySelector('.visible')
	if (currentImage !== lastImage) {
		currentImage.classList.remove('visible')
		currentImage.nextElementSibling.classList.add('visible')
	} else {
		currentImage.classList.remove('visible')
		firstImage.classList.add('visible')
	}
}

let timer = setInterval(changeImg, 3000)

stopButton.addEventListener('click', () => {
	clearInterval(timer)
})

startButton.addEventListener('click', () => {
	clearInterval(timer)
	timer = setInterval(changeImg, 3000)
})





document.querySelector('.burger').addEventListener('click', function (e) {
	e.preventDefault()
	if (this.classList.contains('is-active')) {
		this.classList.remove('is-active')
		document.querySelector('.nav-menu-active__list').style.opacity = '0'
	} else {
		this.classList.add('is-active')
		document.querySelector('.nav-menu-active__list').style.opacity = '1'
	}
})



// Our services

function ourServicesTabs() {
	let tabTitles = document.querySelectorAll('.our-services-menu-item')
	let tabsContent = document.querySelectorAll('.content')

	tabTitles.forEach(item => {
		item.addEventListener('click', selectTabTitle)
	})

	function selectTabTitle() {
		tabTitles.forEach(item => {
			item.classList.remove('active')
		})
		this.classList.add('active')
		let activeTabTitle = this.dataset.title
		selectTabContent(activeTabTitle)
	}

	function selectTabContent(activeTabTitle) {
		tabsContent.forEach(item => {
			if (item.classList.contains(activeTabTitle)) {
				item.classList.add('active-content')
			} else {
				item.classList.remove('active-content')
			}
		})
	}
}
ourServicesTabs()

// Our amazing work

function ourAmazingWorksTabs() {
	let tabTitlesWorks = document.querySelectorAll('.our-amazing-work-menu-item')
	let tabsImgs = document.querySelectorAll('.our-amazing-work-img')

	tabTitlesWorks.forEach(item => {
		item.addEventListener('click', selectTabTitleWorks)
	})

	function selectTabTitleWorks() {
		tabTitlesWorks.forEach(item => {
			item.classList.remove('active-amazing-work-item')
		})
		this.classList.add('active-amazing-work-item')
		let activeTabTitleWorks = this.dataset.list
		selectTabImgsWorks(activeTabTitleWorks)
	}

	function selectTabImgsWorks(activeTabTitleWorks) {
		tabsImgs.forEach(item => {
			if (activeTabTitleWorks === 'all') {
				item.hidden = false
			} else {
				item.hidden = true
				if (item.parentElement.dataset.content === activeTabTitleWorks) {
					item.hidden = false
				}
			}
		})
	}
}
ourAmazingWorksTabs()

document.querySelector('.our-amazing-work-btn').addEventListener('click', function () {
	const allHiddenImgs = document.querySelectorAll('.hidden-img')

	allHiddenImgs.forEach(singleImg => {
		singleImg.classList.remove('hidden-img')
	})
	this.remove()
})

// Slider

let position = 0
const track = document.querySelector('.person-info-track')
const btnPrev = document.getElementById('slider-left-btn')
const btnNext = document.getElementById('slider-right-btn')
const sliderItem = document.querySelectorAll('.slider-item')
const sliderLenght = sliderItem.length

function sliderMotion() {
	const sliderActive = 'slider-item slider-active'
	const classReset = sliderItem.forEach(function (params) {
		params.className = 'slider-item'
	})
	if (position === 0) {
		classReset
		sliderItem[0].className = sliderActive
	}
	if (position === -1160) {
		classReset
		sliderItem[1].className = sliderActive
	}
	if (position === -2320) {
		classReset
		sliderItem[2].className = sliderActive
	}
	if (position === -3480) {
		classReset
		sliderItem[3].className = sliderActive
	}
	track.style.transform = `translateX(${position}px)`
}

sliderItem.forEach(function (person) {
	person.addEventListener('click', () => {
		const id = person.getAttribute('data-name'),
			activePerson = document.querySelector('.slider-item.slider-active')

		activePerson.classList.remove('slider-active')
		person.classList.add('slider-active')

		if (id === 'persona1') {
			position = 0
		}
		if (id === 'persona2') {
			position = -1160
		}
		if (id === 'persona3') {
			position = -2320
		}
		if (id === 'persona4') {
			position = -3480
		}
		sliderMotion()
	})
})

btnPrev.addEventListener('click', () => {
	if (position === 0) {
		position = -3480
	} else {
		position += 1160
	}
	sliderMotion()
})
btnNext.addEventListener('click', () => {
	if (position === -3480) {
		position = 0
	} else {
		position -= 1160
	}
	sliderMotion()
})

import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	isModalOpen: false,
	isButton: true,
	modalProps: {},
};

export const modalSlice = createSlice({
	name: 'modal',
	initialState,
	reducers: {
		toggleModal: (state, action) => {
			state.isModalOpen = action.payload;
		},

		setModalProps: (state, action) => {
			state.modalProps = action.payload;
		},
	},
});

export const { toggleModal, setModalProps } = modalSlice.actions;
export default modalSlice.reducer;

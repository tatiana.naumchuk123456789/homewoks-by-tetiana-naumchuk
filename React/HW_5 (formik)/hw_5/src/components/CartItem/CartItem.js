import React from 'react';
import { useDispatch } from 'react-redux';
import {
	incrementCardItem,
	dicrementCartItem,
} from '../../store/slices/cardSlice';
import { toggleModal, setModalProps } from '../../store/slices/modalSlice';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import delBtn from '../../svg/delete.svg';
import styles from './CartItem.module.scss';

const CartItem = props => {
	const dispatch = useDispatch();
	const { url, article, count, name } = props;

	return (
		<>
			<div className={styles.cartItem}>
				<div className={styles.imgWrapper}>
					<img src={url} alt={name} className={styles.itemAvatar} />
				</div>

				<p className={styles.quantity}>{count}</p>

				<div className={styles.btnCart}>
					<Button
						className={styles.btn}
						type="button"
						text="+"
						onClick={() => {
							dispatch(incrementCardItem(article));
						}}
						backgroundColor="#ff4d4d"
					/>
					<Button
						className={styles.btn}
						type="button"
						text=" – "
						onClick={() => {
							dispatch(dicrementCartItem(article));
						}}
						backgroundColor="#ff4d4d"
					/>
					<button
						style={{
							borderRadius: '5px',
							border: '1px solid #ff4d4d',
							cursor: 'pointer',
						}}
						onClick={() => {
							dispatch(setModalProps({ name }));
							dispatch(toggleModal(true));
						}}
					>
						<img src={delBtn} alt="delete" />
					</button>
				</div>
			</div>
		</>
	);
};

CartItem.propTypes = {
	url: PropTypes.string,
	article: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
	count: PropTypes.number,
	name: PropTypes.string,
};

CartItem.defaultProps = {
	url: './img/1.jpg',
	count: 0,
	name: 'Лосіни',
};

export default CartItem;

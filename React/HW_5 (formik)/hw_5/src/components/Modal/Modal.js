import React from 'react';
import { useDispatch } from 'react-redux';
import { toggleModal } from '../../store/slices/modalSlice';
import PropTypes from 'prop-types';
import styles from './Modal.module.scss';

const Modal = props => {
	const dispatch = useDispatch();
	const handleAddToCart = () => {
		const { modalProps, addToCarts } = props;
		dispatch(addToCarts(modalProps));
		dispatch(toggleModal());
	};

	const handleDeleteFromCart = () => {
		const { delFromCart, modalProps } = props;
		dispatch(delFromCart(modalProps));
		dispatch(toggleModal());
	};

	const { header, closeButton, background, isModalOpen, modalProps } = props;

	if (!isModalOpen) {
		return null;
	}

	return (
		<>
			<div className={styles.wrapper} style={{ background }}>
				<div className={styles.header}>
					<h2 className={styles.headerText}>{header}</h2>
					{closeButton && (
						<button
							className={styles.closeBtn}
							onClick={() => {
								dispatch(toggleModal());
							}}
						>
							X
						</button>
					)}
				</div>

				<div className={styles.body}>
					<div className={styles.bodyBtn}>
						{modalProps.isAdding ? (
							<button className={styles.button} onClick={handleAddToCart}>
								ТАК
							</button>
						) : (
							<button className={styles.button} onClick={handleDeleteFromCart}>
								ТАК
							</button>
						)}
						<button
							className={styles.button}
							onClick={() => {
								dispatch(toggleModal());
							}}
						>
							НІ
						</button>
					</div>
				</div>
			</div>
		</>
	);
};

Modal.propTypes = {
	modalProps: PropTypes.object,
	addToCarts: PropTypes.func,
	delFromCart: PropTypes.func,
	header: PropTypes.string,
	closeButton: PropTypes.bool,
	background: PropTypes.string,
	isModalOpen: PropTypes.bool,
};

Modal.defaultProps = {
	modalProps: {},
	addToCarts: () => {},
	delFromCart: () => {},
	header: 'Ви хочете додати цей товар у корзину?',
	closeButton: true,
	background: '#ff4d4d',
	isModalOpen: false,
};

export default Modal;

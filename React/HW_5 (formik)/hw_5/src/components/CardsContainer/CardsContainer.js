import React from 'react';
import CardItem from '../CardItem/CardItem';
import PropTypes from 'prop-types';
import styles from './CardsContainer.module.scss';

const CardsContainer = props => {
	const { cards, setCardFavorite } = props;

	return (
		<>
			<ul className={styles.list}>
				{cards.map(({ name, prise, url, article, color, isFavorite }) => (
					<li key={article}>
						<CardItem
							name={name}
							prise={`Ціна: ${prise}`}
							url={url}
							article={`Артикул: ${article}`}
							color={`Колір: ${color}`}
							setCardFavorite={setCardFavorite}
							isFavorite={isFavorite}
						/>
					</li>
				))}
			</ul>
		</>
	);
};

CardsContainer.propTypes = {
	cards: PropTypes.array.isRequired,
	setCardFavorite: PropTypes.func.isRequired,
};

export default CardsContainer;

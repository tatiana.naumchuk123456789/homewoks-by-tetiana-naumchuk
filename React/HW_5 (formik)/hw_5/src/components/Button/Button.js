import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.scss';

const Button = props => {
	const { text, onClick, backgroundColor, type } = props;
	return (
		<>
			<button
				type={type}
				className={styles.btn}
				style={{ backgroundColor }}
				onClick={onClick}
			>
				{text}
			</button>
		</>
	);
};

Button.propTypes = {
	text: PropTypes.string,
	onClick: PropTypes.func,
	backgroundColor: PropTypes.string,
	type: PropTypes.string,
};

Button.defaultProps = {
	text: 'Додати у корзину',
	onClick: () => {},
	backgroundColor: 'ff4d4d',
	type: 'button',
};

export default Button;

import React from 'react';
import CartItem from '../CartItem/CartItem';
import PropTypes from 'prop-types';
import styles from './CartContainer.module.scss';

const CartContainer = props => {
	const { carts } = props;
	return (
		<>
			<ul className={styles.list}>
				{carts.map(({ url, article, count, name }) => {
					return (
						<li key={article}>
							<CartItem url={url} article={article} count={count} name={name} />
						</li>
					);
				})}
			</ul>
		</>
	);
};

CartContainer.propTypes = {
	carts: PropTypes.array.isRequired,
};

export default CartContainer;

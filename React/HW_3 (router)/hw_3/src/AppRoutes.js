import { Routes, Route } from 'react-router-dom';
import HomePage from './pages/HomePage';
import Favorites from './pages/Favorites';
import Cart from './pages/Cart';
import PropTypes from 'prop-types';

const AppRoutes = ({
	cards,
	favoriteCards,
	setCardFavorite,
	isModalOpen,
	toggleModal,
	modalProps,
	setModalProps,
	carts,
	incrementCartItem,
	dicrementCartItem,
}) => {
	return (
		<>
			<Routes>
				<Route
					path="/"
					element={
						<HomePage
							cards={cards}
							setCardFavorite={setCardFavorite}
							isModalOpen={isModalOpen}
							toggleModal={toggleModal}
							setModalProps={setModalProps}
						/>
					}
				/>
				<Route
					path="/favourite"
					element={
						<Favorites
							cards={cards}
							favoriteCards={favoriteCards}
							setCardFavorite={setCardFavorite}
							isModalOpen={isModalOpen}
							toggleModal={toggleModal}
							setModalProps={setModalProps}
						/>
					}
				/>
				<Route
					path="/cart"
					element={
						<Cart
							incrementCartItem={incrementCartItem}
							dicrementCartItem={dicrementCartItem}
							carts={carts}
							isModalOpen={isModalOpen}
							toggleModal={toggleModal}
							setModalProps={setModalProps}
							modalProps={modalProps}
						/>
					}
				/>
			</Routes>
		</>
	);
};
AppRoutes.propTypes = {
	cards: PropTypes.array.isRequired,
	carts: PropTypes.array.isRequired,
	favoriteCards: PropTypes.array,
	isModalOpen: PropTypes.bool,
	toggleModal: PropTypes.func,
	modalProps: PropTypes.object,
	setModalProps: PropTypes.func.isRequired,
	incrementCartItem: PropTypes.func.isRequired,
	dicrementCartItem: PropTypes.func.isRequired,
};

AppRoutes.defaultProps = {
	favoriteCards: [],
	isModalOpen: false,
	toggleModal: () => {},
	modalProps: {},
};

export default AppRoutes;

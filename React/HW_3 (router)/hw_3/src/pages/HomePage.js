import React from 'react';
import PropTypes from 'prop-types';
import CardsContainer from '../components/CardsContainer/CardsContainer';

const HomePage = ({
	cards,
	setCardFavorite,
	isModalOpen,
	toggleModal,
	setModalProps,
}) => {
	return (
		<>
			<CardsContainer
				cards={cards}
				setCardFavorite={setCardFavorite}
				isModalOpen={isModalOpen}
				toggleModal={toggleModal}
				setModalProps={setModalProps}
			/>
		</>
	);
};

HomePage.propTypes = {
	cards: PropTypes.array.isRequired,
	setCardFavorite: PropTypes.func.isRequired,
	isModalOpen: PropTypes.bool,
	toggleModal: PropTypes.func,
	setModalProps: PropTypes.func.isRequired,
};

HomePage.defaultProps = {
	isModalOpen: false,
	toggleModal: () => {},
};

export default HomePage;

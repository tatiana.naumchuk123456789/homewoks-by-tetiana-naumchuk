import React, { useState, useEffect } from 'react';
import Header from './components/Header/Header';
import AppRoutes from './AppRoutes';
import Modal from './components/Modal/Modal';
import styles from './App.module.scss';

const App = () => {
	const [cards, setCards] = useState([]);
	const [carts, setCarts] = useState([]);
	const [favoriteCards, setFavoriteCards] = useState([]);
	const [isModalOpen, setIsModalOpen] = useState(false);
	const [isButton, setIsButton] = useState(true);
	const [modalProps, setModalProps] = useState({});

	useEffect(() => {
		const dataWrapper = async () => {
			try {
				let data = await fetch('./data.json').then(res => res.json());
				const carts = localStorage.getItem('carts');
				if (carts) {
					setCarts(JSON.parse(carts));
				}
				setCards(data);

				const favArray = localStorage.getItem('favoriteCards');
				if (favArray) {
					data = data.map(card => {
						if (favArray.includes(card.url)) {
							card.isFavorite = true;
						}
						return card;
					});
					setFavoriteCards(JSON.parse(favArray));
				}
				setCards(data);
			} catch (error) {
				console.error();
			}
		};
		dataWrapper();
	}, []);

	const addToCarts = card => {
		setCarts(current => {
			const carts = [...current];

			const index = carts.findIndex(el => el.article === card.article);

			if (index === -1) {
				carts.push({ ...card, count: 1 });
			} else {
				carts[index].count += 1;
			}

			localStorage.setItem('carts', JSON.stringify(carts));

			return carts;
		});
	};

	const incrementCartItem = article => {
		setCarts(current => {
			const carts = [...current];

			const index = carts.findIndex(el => el.article === article);

			if (index !== -1) {
				carts[index].count += 1;
			}

			localStorage.setItem('carts', JSON.stringify(carts));

			return carts;
		});
	};

	const dicrementCartItem = article => {
		setCarts(current => {
			const carts = [...current];

			const index = carts.findIndex(el => el.article === article);

			if (index !== -1) {
				carts[index].count -= 1;
			}

			if (carts[index].count < 1) {
				carts.splice(index, 1);
			}

			localStorage.setItem('carts', JSON.stringify(carts));

			return carts;
		});
	};

	const delFromCart = article => {
		setCarts(current => {
			const carts = [...current];

			const index = carts.findIndex(el => el.article === article);

			if (index === -1) {
				carts.splice(index, 1);
			}

			localStorage.setItem('carts', JSON.stringify(carts));

			return carts;
		});
	};

	const setCardFavorite = url => {
		setFavoriteCards(prevFavoriteCards => {
			const favArray = [...prevFavoriteCards];
			let updatedCards = [...cards];
			const index = favArray.findIndex(item => item.url === url);

			if (index === -1) {
				favArray.push({ url, count: 1 });
			} else {
				favArray.splice(index, 1);
			}

			updatedCards = updatedCards.map(card => {
				if (card.url === url) {
					card.isFavorite = !card.isFavorite;
				}
				return card;
			});

			localStorage.setItem('favoriteCards', JSON.stringify(favArray));
			setCards(updatedCards);
			return favArray;
		});
	};

	const openModal = () => {
		setIsModalOpen(true);
		setIsButton(false);
	};

	const toggleModal = value => {
		setIsModalOpen(value);
	};

	const setModalPropsFunc = value => {
		setModalProps(value);
	};

	return (
		<>
			<Header carts={carts} favoriteCards={favoriteCards} />
			<main>
				<AppRoutes
					cards={cards}
					favoriteCards={favoriteCards}
					setCardFavorite={setCardFavorite}
					isModalOpen={isModalOpen}
					toggleModal={toggleModal}
					carts={carts}
					setModalProps={setModalProps}
					incrementCartItem={incrementCartItem}
					dicrementCartItem={dicrementCartItem}
					addToCarts={addToCarts}
					modalProps={modalProps}
				/>

				<Modal
					isModalOpen={isModalOpen}
					toggleModal={toggleModal}
					background="#ff4d4d"
					header={
						modalProps.isAdding
							? `Ви хочете додати "${modalProps.name}" у корзину?`
							: `Ви хочете видалити "${modalProps.name}" з корзини?`
					}
					closeButton={true}
					addToCarts={addToCarts}
					modalProps={modalProps}
					delFromCart={delFromCart}
				/>
			</main>
		</>
	);
};

export default App;

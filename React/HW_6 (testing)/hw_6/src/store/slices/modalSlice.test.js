import '@testing-library/jest-dom/extend-expect';
import { configureStore } from '@reduxjs/toolkit';
import cardSlice from './cardSlice';
import { toggleModal } from './modalSlice';
import { modalSlice } from './modalSlice';

describe('modalReducer', () => {
	let store;

	beforeEach(() => {
		store = configureStore({
			reducer: {
				cards: cardSlice,
				modal: modalSlice,
			},
		});
	});
	it('should toogle Modal', () => {
		const initialState = {
			isModalOpen: false,
			isButton: true,
			modalProps: {},
		};
		const nextState = modalSlice.reducer(initialState, toggleModal(true));
		expect(nextState).toEqual({
			isModalOpen: true,
			isButton: true,
			modalProps: {},
		});
	});
});

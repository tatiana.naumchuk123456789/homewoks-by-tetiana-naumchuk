import '@testing-library/jest-dom/extend-expect';
import { configureStore } from '@reduxjs/toolkit';
import cardSlice, { addToCarts } from './cardSlice';
import modalSlice from './modalSlice';

describe('cardReducer', () => {
	let store;

	beforeEach(() => {
		store = configureStore({
			reducer: {
				cards: cardSlice,
				modal: modalSlice,
			},
		});
	});
	it('should increment the counter', () => {
		store.dispatch(addToCarts({ article: 236578 }));
		expect(store.getState().cards).toStrictEqual({
			cards: [],
			carts: [{ article: 236578, count: 1 }],
			favorite: [],
		});
	});
});

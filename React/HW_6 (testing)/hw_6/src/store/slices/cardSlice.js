import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
	cards: [],
	carts: localStorage.getItem('carts')
		? JSON.parse(localStorage.getItem('carts'))
		: [],
	favorite: localStorage.getItem('favoriteCards')
		? JSON.parse(localStorage.getItem('favoriteCards'))
		: [],
};

export const getCards = createAsyncThunk('cards/getCards', async () => {
	try {
		let data = await fetch('./data.json').then(res => res.json());
		return data;
	} catch (error) {
		console.log(error);
	}
});

export const cardSlice = createSlice({
	name: 'cards',
	initialState,
	reducers: {
		setCards: (state, action) => {
			state.cards = action.payload;
		},

		addToCarts: (state, action) => {
			const card = action.payload;
			const index = state.carts.findIndex(el => el.article === card.article);
			if (index === -1) {
				state.carts.push({ ...card, count: 1 });
			} else {
				state.carts[index].count += 1;
			}
			localStorage.setItem('carts', JSON.stringify(state.carts));
		},

		delFromCarts: (state, action) => {
			const article = action.payload;
			const index = state.carts.findIndex(el => el.article === article);
			if (index === -1) {
				state.carts.splice(index, 1);
			}
			localStorage.setItem('carts', JSON.stringify(state.carts));
		},

		setCardFavorite: (state, action) => {
			const url = action.payload;
			const favArray = state.favorite;
			let updatedCards = state.cards;
			const index = favArray.findIndex(item => item.url === url);
			if (index === -1) {
				favArray.push({ url, count: 1 });
			} else {
				favArray.splice(index, 1);
			}
			updatedCards = updatedCards.map(card => {
				if (card.url === url) {
					card.isFavorite = !card.isFavorite;
				}
				return card;
			});
			localStorage.setItem('favoriteCards', JSON.stringify(favArray));
			state.cards = updatedCards;
		},

		incrementCardItem: (state, action) => {
			const article = action.payload;
			const index = state.carts.findIndex(el => el.article === article);
			if (index !== -1) {
				state.carts[index].count += 1;
			}
			localStorage.setItem('carts', JSON.stringify(state.carts));
		},

		dicrementCartItem: (state, action) => {
			const article = action.payload;
			const index = state.carts.findIndex(el => el.article === article);
			if (index !== -1) {
				state.carts[index].count -= 1;
			}
			if (state.carts[index].count < 1) {
				state.carts.splice(index, 1);
			}
			localStorage.setItem('carts', JSON.stringify(state.carts));
		},

		cleanCart: () => {
			localStorage.removeItem('carts');
		},
	},

	extraReducers: {
		[getCards.fulfilled]: (state, action) => {
			const favArray = localStorage.getItem('favoriteCards');
			let data;
			if (favArray) {
				data = action.payload.map(card => {
					if (favArray.includes(card.url)) {
						card.isFavorite = true;
					}
					return card;
				});
			} else {
				data = action.payload;
			}
			state.cards = data;
		},
	},
});

export const {
	setCards,
	addToCarts,
	delFromCarts,
	setCardFavorite,
	incrementCardItem,
	dicrementCartItem,
	cleanCart,
} = cardSlice.actions;
export default cardSlice.reducer;

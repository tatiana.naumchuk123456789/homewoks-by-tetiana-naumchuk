import React from 'react';
import { Formik, Form, Field } from 'formik';
import * as yup from 'yup';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import { cleanCart } from '../../store/slices/cardSlice';
// import { PatternFormat } from 'react-number-format';
import styles from './FormOrder.module.scss';

const FormOrder = ({ carts }) => {
	const dispatch = useDispatch();

	const initialValues = {
		name: '',
		surname: '',
		age: '',
		address: '',
		phone: '+380',
	};

	const validationSchema = yup.object().shape({
		name: yup
			.string()
			.min(2, "Ім'я занадто коротке")
			.max(25, "Ім'я занадто довге")
			.required("Поле обов'язкове для заповнення")
			.matches(/^([^0-9]*)$/gm, 'Тільки букви'),
		surname: yup
			.string()
			.min(2, 'Прізвище занадто коротке')
			.max(25, 'Прізвище занадто довге')
			.required("Поле обов'язкове для заповнення")
			.matches(/^([^0-9]*)$/gm, 'Тільки букви'),
		age: yup
			.number()
			.integer('Тільки ціле число')
			.min(16, 'Вам ще немає 16')
			.max(100, 'Введіть коректний вік')
			.required("Поле обов'язкове для заповнення"),
		address: yup
			.string()
			.min(10, 'Занадто коротка адреса')
			.max(100, 'Занадто довга адреса')
			.required("Поле обов'язкове для заповнення"),
		phone: yup
			.string()
			// .matches([/\d+/g], 'Тільки цифри')
			.required("Поле обов'язкове для заповнення"),
	});

	const consoleData = carts
		.map(({ name, count }) => {
			return name + ' - ' + count + 'шт.';
		})
		.join('\n');

	return (
		<Formik
			initialValues={initialValues}
			onSubmit={(values, { resetForm }) => {
				console.log(
					'Ваше замовлення:\n' +
						consoleData +
						'\nДані:\n' +
						Object.values(values).join(' , '),
				);
				dispatch(cleanCart());
				resetForm();
			}}
			validationSchema={validationSchema}
			validateOnBlur={true}
		>
			{({ touched, errors, isValid, values }) => {
				const disabled =
					Object.keys(touched).length !== Object.keys(values).length &&
					Object.values(values).includes('');

				return (
					<Form>
						<h3>Введіть дані для відправки:</h3>
						<label>
							Ім'я:
							<Field type="text" name="name" />
							{touched.name && errors.name && (
								<span className={styles.error}>{errors.name}</span>
							)}
						</label>
						<label>
							Прізвище:
							<Field type="text" name="surname" />
							{touched.surname && errors.surname && (
								<span className={styles.error}>{errors.surname}</span>
							)}
						</label>
						<label>
							Вік:
							<Field type="text" name="age" />
							{touched.age && errors.age && (
								<span className={styles.error}>{errors.age}</span>
							)}
						</label>
						<label>
							Адреса доставки:
							<Field type="text" name="address" />
							{touched.address && errors.address && (
								<span className={styles.error}>{errors.address}</span>
							)}
						</label>
						<label>
							Мобільний телефон:
							<Field type="text" name="phone" />
							{touched.phone && errors.phone && (
								<span className={styles.error}>{errors.phone}</span>
							)}
						</label>
						<Button
							disabled={disabled ? true : !isValid}
							className={styles.btn}
							type="submit"
							text="Checkout"
							onClick={() => {}}
							backgroundColor="#ff4d4d"
						/>
					</Form>
				);
			}}
		</Formik>
	);
};

FormOrder.propTypes = {
	carts: PropTypes.array.isRequired,
};

export default FormOrder;

import { render, screen, fireEvent } from '@testing-library/react';
import Button from './Button';

describe('Button testing snapshot', () => {
	test('should Button match snapshot', () => {
		const { asFragment } = render(
			<Button text="BUTTON" backgroundColor="aqua" type="submit" />,
		);
		expect(asFragment()).toMatchSnapshot();
	});
});

describe('Button works', () => {
	test('should Button render with provided text', () => {
		render(<Button text="BUTTON" backgroundColor="aqua" type="submit" />);
		expect(screen.getByText('BUTTON')).toBeInTheDocument();
	});
	test('should onClick function call when clicked', () => {
		const onClickMock = jest.fn();
		render(
			<Button
				text="BUTTON"
				backgroundColor="aqua"
				type="submit"
				onClick={onClickMock}
			/>,
		);
		const buttonElem = screen.getByText('BUTTON');
		fireEvent.click(buttonElem);
		expect(onClickMock).toHaveBeenCalled();
	});
});

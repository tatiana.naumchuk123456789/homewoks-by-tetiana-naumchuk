import PropTypes from 'prop-types';
import TableItem from '../TableItem/TableItem';
import styles from './Table.module.scss';

const Table = ({ cards }) => {
	return (
		<table className={styles.tableContainer}>
			<thead>
				<tr>
					<th>Зображення</th>
					<th>Назва</th>
					<th>Код</th>
					<th>Колір</th>
					<th>Ціна</th>
					<th>Дії</th>
				</tr>
			</thead>
			<tbody>
				{cards.map(card => {
					const { article } = card;
					return (
						<tr className={styles.tableLine} key={article}>
							<TableItem card={card} />
						</tr>
					);
				})}
			</tbody>
		</table>
	);
};

Table.propTypes = {
	cards: PropTypes.array.isRequired,
};

export default Table;

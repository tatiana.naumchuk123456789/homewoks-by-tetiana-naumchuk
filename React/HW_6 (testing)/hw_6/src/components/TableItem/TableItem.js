import PropTypes from 'prop-types';
import { setCardFavorite } from '../../store/slices/cardSlice';
import { useDispatch } from 'react-redux';
import { toggleModal, setModalProps } from '../../store/slices/modalSlice';
import Button from '../Button/Button';
import star from '../../svg/star.svg';
import starFill from '../../svg/star_fill.svg';
import styles from './TableItem.module.scss';

const TableItem = ({ card }) => {
	const { name, url, article, color, prise, isFavorite } = card;
	const dispatch = useDispatch();

	const handleAddToCart = () => {
		dispatch(setModalProps({ article, name, url, isAdding: true }));
		dispatch(toggleModal(true));
	};

	return (
		<>
			<td className={styles.imgCell}>
				<img className={styles.itemImg} src={url} alt={name} />
			</td>
			<td>{name}</td>
			<td>{article}</td>
			<td>{color.toUpperCase()}</td>
			<td style={{ fontWeight: 'bold' }}>{prise}</td>
			<td className={styles.actionsCell}>
				<div style={{ display: 'block' }}>
					<button
						style={{ marginBottom: '5px' }}
						type="button"
						className={styles.btn}
						onClick={() => {
							dispatch(setCardFavorite(url));
						}}
					>
						<img
							data-testid="favorite"
							className={styles.likeButton}
							src={!isFavorite ? star : starFill}
							alt="favorite"
						/>
					</button>
					<Button
						type="button"
						text="Додати у корзину"
						backgroundColor="#ff4d4d"
						onClick={handleAddToCart}
					/>
				</div>
			</td>
		</>
	);
};

TableItem.propTypes = {
	card: PropTypes.object.isRequired,
};

export default TableItem;

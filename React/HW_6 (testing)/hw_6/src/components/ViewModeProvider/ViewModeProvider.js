import { createContext, useState } from 'react';

const ViewModeContext = createContext();

const ViewModeProvider = ({ children }) => {
	const [viewMode, setViewMode] = useState('cards');

	return (
		<ViewModeContext.Provider value={{ viewMode, setViewMode }}>
			{children}
		</ViewModeContext.Provider>
	);
};

export { ViewModeContext, ViewModeProvider };

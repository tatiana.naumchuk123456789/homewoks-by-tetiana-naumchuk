import { render } from '@testing-library/react';
import CardItem from './CardItem';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import modalSlice from '../../store/slices/modalSlice';
import cardSlice from '../../store/slices/cardSlice';

describe('CardItem testing snapshot', () => {
	const props = {
		name: 'Костюм топ + шорти',
		prise: '1400 UAH',
		url: './img/1.jpg',
		article: 236578,
		color: 'оранжевий',
	};
	test('should CardItem match snapshot', () => {
		const store = configureStore({
			reducer: {
				cards: cardSlice,
				modal: modalSlice,
			},
		});
		const { asFragment } = render(
			<Provider store={store}>
				<CardItem {...props} />
			</Provider>,
		);
		expect(asFragment()).toMatchSnapshot();
	});
});

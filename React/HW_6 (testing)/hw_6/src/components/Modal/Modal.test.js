import { render } from '@testing-library/react';
import Modal from './Modal';
import { Provider } from 'react-redux';
import cardSlice from '../../store/slices/cardSlice';
import modalSlice from '../../store/slices/modalSlice';
import { configureStore } from '@reduxjs/toolkit';

describe('Modal testing snapshot', () => {
	test('should Modal match snapshot', () => {
		const store = configureStore({
			reducer: {
				cards: cardSlice,
				modal: modalSlice,
			},
		});
		const { asFragment } = render(
			<Provider store={store}>
				<Modal header="Ви хочете додати цей товар у корзину?" />
			</Provider>,
		);
		expect(asFragment()).toMatchSnapshot();
	});
});

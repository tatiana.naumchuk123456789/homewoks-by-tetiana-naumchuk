import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import CardsContainer from '../components/CardsContainer/CardsContainer';
import Table from '../components/Table/Table';
import { ViewModeContext } from '../components/ViewModeProvider/ViewModeProvider';
import styles from '../App.module.scss';

const HomePage = () => {
	// const HomePage = ({ cards, setCardFavorite }) => {
	const cards = useSelector(store => store.cards.cards);
	const { viewMode, setViewMode } = useContext(ViewModeContext);
	return (
		<>
			<div className={styles.toggleView}>
				<img
					className={styles.toggleViewTable}
					onClick={() => {
						setViewMode('table');
					}}
					src="./img/table-view.png"
					alt="table-view"
				/>
				<img
					className={styles.toggleViewCards}
					onClick={() => {
						setViewMode('cards');
					}}
					src="./img/cards-view.png"
					alt="cards-view"
				/>
			</div>
			{viewMode === 'cards' && <CardsContainer cards={cards} />}
			{viewMode === 'table' && <Table cards={cards} />}
			{/* <CardsContainer cards={cards} setCardFavorite={setCardFavorite} /> */}
		</>
	);
};

HomePage.propTypes = {
	cards: PropTypes.array.isRequired,
	setCardFavorite: PropTypes.func.isRequired,
};

export default HomePage;

import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.scss';

const Button = props => {
	const { text, onClick, backgroundColor } = props;
	return (
		<>
			<button
				type="button"
				className={styles.btn}
				style={{ backgroundColor }}
				onClick={onClick}
			>
				{text}
			</button>
		</>
	);
};

Button.propTypes = {
	text: PropTypes.string,
	onClick: PropTypes.func,
	backgroundColor: PropTypes.string,
};

Button.defaultProps = {
	text: 'Додати у корзину',
	onClick: () => {},
	backgroundColor: 'ff4d4d',
};

export default Button;

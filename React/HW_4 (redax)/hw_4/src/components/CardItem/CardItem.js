import React from 'react';
import { useDispatch } from 'react-redux';
import { setCardFavorite } from '../../store/slices/cardSlice';
import { toggleModal, setModalProps } from '../../store/slices/modalSlice';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import styles from './CardItem.module.scss';
import star from '../../svg/star.svg';
import starFill from '../../svg/star_fill.svg';

const CardItem = props => {
	const dispatch = useDispatch();

	const handleAddToCart = () => {
		const { article, name, url } = props;
		dispatch(setModalProps({ article, name, url, isAdding: true }));
		dispatch(toggleModal(true));
	};

	const { name, prise, url, article, color, isFavorite } = props;

	return (
		<>
			<div className={styles.card}>
				<button
					type="button"
					className={styles.btn}
					onClick={() => {
						dispatch(setCardFavorite(url));
					}}
				>
					<img
						className={styles.likeButton}
						src={!isFavorite ? star : starFill}
						alt="favorite"
					/>
				</button>

				<h4 className={styles.name}>{name}</h4>
				<img src={url} alt={name} className={styles.img} />
				<h5 className={styles.prise}>{prise}</h5>
				<h5 className={styles.article}>{article}</h5>
				<h5 className={styles.color}>{color}</h5>
				<div className={styles.btnContainer}>
					<Button
						text="Додати у корзину"
						backgroundColor="#ff4d4d"
						onClick={handleAddToCart}
					/>
				</div>
			</div>
		</>
	);
};

CardItem.propTypes = {
	name: PropTypes.string,
	price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	url: PropTypes.string,
	article: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
	color: PropTypes.string,
	isFavourite: PropTypes.bool,
};

CardItem.defaultProps = {
	name: 'Лосіни',
	price: '0',
	url: './img/1.jpg',
	color: 'чорний',
	isFavourite: false,
};

export default CardItem;

import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import styles from './Header.module.scss';

const Header = props => {
	const { carts, favoriteCards } = props;
	const totalQuantity = carts.reduce((acc, { count }) => acc + count, 0);
	const totalQuantityHeart = favoriteCards.reduce(
		(acc, { count }) => acc + count,
		0,
	);

	return (
		<>
			<nav className={styles.wrapper}>
				<div className={styles.logo}>
					<img src={'/img/logo.jpg'} alt="logo" className={styles.logoImg} />
				</div>

				<div className={styles.text}>
					<NavLink className={styles.title} to="/">
						WW<span>W</span>elcome
					</NavLink>
					<NavLink className={styles.heartSvg} to="/favourite">
						<svg
							xmlns="http://www.w3.org/2000/svg"
							width="34"
							height="34"
							fill="darkgrey"
							viewBox="0 0 24 24"
						>
							<path d="M12 4.435c-1.989-5.399-12-4.597-12 3.568 0 4.068 3.06 9.481 12 14.997 8.94-5.516 12-10.929 12-14.997 0-8.118-10-8.999-12-3.568z" />
						</svg>

						<div className={styles.heartCount}>
							{totalQuantityHeart > 0 && (
								<p className={styles.countHeart}>{totalQuantityHeart}</p>
							)}
						</div>
					</NavLink>
				</div>
				<NavLink className={styles.cartSvg} to="/cart">
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="50"
						height="50"
						fill="white"
						className="bi bi-cart3"
						viewBox="0 0 16 16"
					>
						{' '}
						<path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />{' '}
					</svg>
					<div className={styles.cartCount}>
						{totalQuantity > 0 && (
							<p className={styles.count}>{totalQuantity}</p>
						)}
					</div>
				</NavLink>
			</nav>
		</>
	);
};

Header.propTypes = {
	carts: PropTypes.array.isRequired,
	favoriteCards: PropTypes.array.isRequired,
};

export default Header;

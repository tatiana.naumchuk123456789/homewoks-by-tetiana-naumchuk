import { Routes, Route } from 'react-router-dom';
import HomePage from './pages/HomePage';
import Favorites from './pages/Favorites';
import Cart from './pages/Cart';
import PropTypes from 'prop-types';

const AppRoutes = ({ cards, favoriteCards, setCardFavorite, carts }) => {
	return (
		<>
			<Routes>
				<Route
					path="/"
					element={<HomePage cards={cards} setCardFavorite={setCardFavorite} />}
				/>
				<Route
					path="/favourite"
					element={
						<Favorites
							cards={cards}
							favoriteCards={favoriteCards}
							setCardFavorite={setCardFavorite}
						/>
					}
				/>
				<Route path="/cart" element={<Cart carts={carts} />} />
			</Routes>
		</>
	);
};
AppRoutes.propTypes = {
	cards: PropTypes.array.isRequired,
	carts: PropTypes.array.isRequired,
	favoriteCards: PropTypes.array,
	setCardFavorite: PropTypes.func.isRequired,
};

AppRoutes.defaultProps = {
	favoriteCards: [],
};

export default AppRoutes;

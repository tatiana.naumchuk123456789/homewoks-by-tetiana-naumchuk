import React from 'react';
import CartContainer from '../components/CartContainer/CartContainer';
import PropTypes from 'prop-types';

const Cart = ({ carts }) => {
	return (
		<div style={{ paddingBottom: '100%' }}>
			<h1 style={{ textAlign: 'center' }}>Корзина</h1>
			{carts.length === 0 ? (
				<p style={{ textAlign: 'center' }}>У Вас немає товарів у cart</p>
			) : (
				<CartContainer carts={carts} />
			)}
		</div>
	);
};

Cart.propTypes = {
	carts: PropTypes.array.isRequired,
};

export default Cart;

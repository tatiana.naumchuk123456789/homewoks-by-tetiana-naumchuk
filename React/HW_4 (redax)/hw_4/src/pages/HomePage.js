import React from 'react';
import PropTypes from 'prop-types';
import CardsContainer from '../components/CardsContainer/CardsContainer';

const HomePage = ({ cards, setCardFavorite }) => {
	return (
		<>
			<CardsContainer cards={cards} setCardFavorite={setCardFavorite} />
		</>
	);
};

HomePage.propTypes = {
	cards: PropTypes.array.isRequired,
	setCardFavorite: PropTypes.func.isRequired,
};

export default HomePage;

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Header from './components/Header/Header';
import AppRoutes from './AppRoutes';
import Modal from './components/Modal/Modal';

import {
	getCards,
	addToCarts,
	delFromCarts,
	setCardFavorite,
} from './store/slices/cardSlice';

import styles from './App.module.scss';

const App = () => {
	const dispatch = useDispatch();

	const cards = useSelector(store => store.cards.cards);
	const carts = useSelector(store => store.cards.carts);
	const favorite = useSelector(store => store.cards.favorite);
	const isModalOpen = useSelector(store => store.modal.isModalOpen);
	const modalProps = useSelector(store => store.modal.modalProps);

	useEffect(() => {
		dispatch(getCards());
	}, [dispatch]);

	return (
		<>
			<Header carts={carts} favoriteCards={favorite} />
			<main>
				<AppRoutes
					cards={cards}
					favoriteCards={favorite}
					setCardFavorite={setCardFavorite}
					carts={carts}
				/>

				<Modal
					isModalOpen={isModalOpen}
					background="#ff4d4d"
					header={
						modalProps.isAdding
							? `Ви хочете додати "${modalProps.name}" у корзину?`
							: `Ви хочете видалити "${modalProps.name}" з корзини?`
					}
					closeButton={true}
					addToCarts={addToCarts}
					modalProps={modalProps}
					delFromCart={delFromCarts}
				/>
			</main>
		</>
	);
};

export default App;

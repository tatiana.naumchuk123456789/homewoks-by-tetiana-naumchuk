import { configureStore } from '@reduxjs/toolkit';
import modalSlice from './slices/modalSlice';
import cardSlice from './slices/cardSlice';

const store = configureStore({
	reducer: {
		cards: cardSlice,
		modal: modalSlice,
	},
});

export default store;

import React from 'react';
import CartItem from '../CartItem/CartItem';
import PropTypes from 'prop-types';
import styles from './CartContainer.module.scss';

const CartContainer = props => {
	const { carts, incrementCartItem, dicrementCartItem } = props;
	return (
		<>
			<ul className={styles.list}>
				{carts.map(({ url, article, count, name }) => {
					return (
						<li key={article}>
							<CartItem
								url={url}
								article={article}
								count={count}
								name={name}
								incrementCartItem={incrementCartItem}
								dicrementCartItem={dicrementCartItem}
							/>
						</li>
					);
				})}
			</ul>
		</>
	);
};

CartContainer.propTypes = {
	carts: PropTypes.array.isRequired,
	incrementCartItem: PropTypes.func.isRequired,
	dicrementCartItem: PropTypes.func.isRequired,
};

export default CartContainer;

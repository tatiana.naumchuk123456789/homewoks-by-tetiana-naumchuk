import React from 'react';
import CardItem from '../CardItem/CardItem';
import PropTypes from 'prop-types';
import styles from './CardsContainer.module.scss';

const CardsContainer = props => {
	const { cards, setCardFavorite, toggleModal, setModalProps } = props;

	return (
		<>
			<ul className={styles.list}>
				{cards.map(({ name, prise, url, article, color, isFavorite }) => (
					<li key={article}>
						<CardItem
							name={name}
							prise={`Ціна: ${prise}`}
							url={url}
							article={`Артикул: ${article}`}
							color={`Колір: ${color}`}
							setCardFavorite={setCardFavorite}
							isFavorite={isFavorite}
							toggleModal={toggleModal}
							setModalProps={setModalProps}
						/>
					</li>
				))}
			</ul>
		</>
	);
};

CardsContainer.propTypes = {
	cards: PropTypes.array.isRequired,
	setCardFavorite: PropTypes.func.isRequired,
	toggleModal: PropTypes.func,
	setModalProps: PropTypes.func.isRequired,
};

CardsContainer.defaultProps = {
	toggleModal: () => {},
};
export default CardsContainer;

import React from 'react';
import CartContainer from '../components/CartContainer/CartContainer';
import PropTypes from 'prop-types';

const Cart = ({
	incrementCartItem,
	dicrementCartItem,
	carts,
	modalProps,
	setModalProps,
	isModalOpen,
	toggleModal,
}) => {
	return (
		<div style={{ paddingBottom: '100%' }}>
			<h1 style={{ textAlign: 'center' }}>Корзина</h1>
			{carts.length === 0 ? (
				<p style={{ textAlign: 'center' }}>У Вас немає товарів у cart</p>
			) : (
				<CartContainer
					incrementCartItem={incrementCartItem}
					dicrementCartItem={dicrementCartItem}
					carts={carts}
					modalProps={modalProps}
					setModalProps={setModalProps}
					isModalOpen={isModalOpen}
					toggleModal={toggleModal}
				/>
			)}
		</div>
	);
};

Cart.propTypes = {
	incrementCartItem: PropTypes.func.isRequired,
	dicrementCartItem: PropTypes.func.isRequired,
	carts: PropTypes.array.isRequired,
	modalProps: PropTypes.object,
	setModalProps: PropTypes.func.isRequired,
	isModalOpen: PropTypes.bool,
	toggleModal: PropTypes.func,
};

Cart.defaultProps = {
	modalProps: {},
	isModalOpen: false,
	toggleModal: () => {},
};

export default Cart;

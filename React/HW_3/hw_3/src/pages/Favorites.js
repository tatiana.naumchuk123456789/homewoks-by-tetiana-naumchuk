import React from 'react';
import CardsContainer from '../components/CardsContainer/CardsContainer';
import PropTypes from 'prop-types';

const Favorites = ({
	favoriteCards,
	cards,
	isModalOpen,
	toggleModal,
	setModalProps,
	setCardFavorite,
}) => {
	let favorites = [];
	favoriteCards.forEach(item => {
		cards.forEach(card => {
			if (card.url === item.url) {
				favorites.push(card);
			}
		});
	});

	const styleText = {
		textAlign: 'center',
	};

	return (
		<>
			<div style={{ paddingBottom: '100%' }}>
				<h1 style={styleText}>Вибране</h1>
				{favoriteCards.length === 0 ? (
					<p style={styleText}>У Вас немає товарів у favorites</p>
				) : (
					<CardsContainer
						setModalProps={setModalProps}
						setCardFavorite={setCardFavorite}
						cards={favorites}
						isModalOpen={isModalOpen}
						toggleModal={toggleModal}
					/>
				)}
			</div>
		</>
	);
};

Favorites.propTypes = {
	favoriteCards: PropTypes.array,
	cards: PropTypes.array.isRequired,
	isModalOpen: PropTypes.bool,
	toggleModal: PropTypes.func,
	setModalProps: PropTypes.func.isRequired,
	setCardFavorite: PropTypes.func.isRequired,
};

Favorites.defaultProps = {
	favoriteCards: [],
	isModalOpen: false,
	toggleModal: () => {},
};

export default Favorites;

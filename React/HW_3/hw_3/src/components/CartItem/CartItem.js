import React from 'react';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import delBtn from '../../svg/delete.svg';
import styles from './CartItem.module.scss';

const CartItem = props => {
	const {
		url,
		article,
		count,
		name,
		incrementCartItem,
		dicrementCartItem,
		setModalProps,
		toggleModal,
	} = props;

	return (
		<>
			<div className={styles.cartItem}>
				<div className={styles.imgWrapper}>
					<img src={url} alt={name} className={styles.itemAvatar} />
				</div>

				<p className={styles.quantity}>{count}</p>

				<div className={styles.btnCart}>
					<Button
						className={styles.btn}
						text="+"
						onClick={() => {
							incrementCartItem(article);
						}}
						backgroundColor="#ff4d4d"
					/>
					<Button
						className={styles.btn}
						text=" – "
						onClick={() => {
							dicrementCartItem(article);
						}}
						backgroundColor="#ff4d4d"
					/>
					<button
						style={{
							borderRadius: '5px',
							border: '1px solid #ff4d4d',
							cursor: 'pointer',
						}}
						onClick={() => {
							setModalProps({ name });
							toggleModal(true);
						}}
					>
						<img src={delBtn} alt="delete" />
					</button>
				</div>
			</div>
		</>
	);
};

CartItem.propTypes = {
	url: PropTypes.string,
	article: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
	count: PropTypes.number,
	name: PropTypes.string,
	incrementCartItem: PropTypes.func.isRequired,
	dicrementCartItem: PropTypes.func.isRequired,
	setModalProps: PropTypes.func.isRequired,
	toggleModal: PropTypes.func,
};

CartItem.defaultProps = {
	url: './img/1.jpg',
	count: 0,
	name: 'Лосіни',
	toggleModal: () => {},
};

export default CartItem;

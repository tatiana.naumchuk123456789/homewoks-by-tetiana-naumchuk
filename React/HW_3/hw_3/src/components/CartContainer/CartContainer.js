import React from 'react';
import CartItem from '../CartItem/CartItem';
import PropTypes from 'prop-types';
import styles from './CartContainer.module.scss';

const CartContainer = props => {
	const {
		carts,
		incrementCartItem,
		dicrementCartItem,
		modalProps,
		setModalProps,
		isModalOpen,
		toggleModal,
	} = props;
	return (
		<>
			<ul className={styles.list}>
				{carts.map(({ url, article, count, name }) => {
					return (
						<li key={article}>
							<CartItem
								url={url}
								article={article}
								count={count}
								name={name}
								incrementCartItem={incrementCartItem}
								dicrementCartItem={dicrementCartItem}
								setModalProps={setModalProps}
								modalProps={modalProps}
								isModalOpen={isModalOpen}
								toggleModal={toggleModal}
							/>
						</li>
					);
				})}
			</ul>
		</>
	);
};

CartContainer.propTypes = {
	carts: PropTypes.array.isRequired,
	incrementCartItem: PropTypes.func.isRequired,
	dicrementCartItem: PropTypes.func.isRequired,
	modalProps: PropTypes.object,
	setModalProps: PropTypes.func.isRequired,
	isModalOpen: PropTypes.bool,
	toggleModal: PropTypes.func,
};

CartContainer.defaultProps = {
	modalProps: {},
	isModalOpen: false,
	toggleModal: () => {},
};

export default CartContainer;

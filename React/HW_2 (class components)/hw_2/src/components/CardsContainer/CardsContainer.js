import React, { Component } from 'react';
import CardItem from '../CardItem/CardItem';
import PropTypes from 'prop-types';
import styles from './CardsContainer.module.scss';

class CardsContainer extends Component {
	render() {
		const { cards, setCardFavorite, toggleModal, setModalProps } = this.props;

		return (
			<>
				<ul className={styles.list}>
					{cards.map(({ name, prise, url, article, color, isFavorite }) => (
						<li key={article}>
							<CardItem
								name={name}
								prise={`Ціна: ${prise}`}
								url={url}
								article={`Артикул: ${article}`}
								color={`Колір: ${color}`}
								setCardFavorite={setCardFavorite}
								isFavorite={isFavorite}
								toggleModal={toggleModal}
								setModalProps={setModalProps}
							/>
						</li>
					))}
				</ul>
			</>
		);
	}
}

CardsContainer.propTypes = {
	cards: PropTypes.array.isRequired,
	setCardFavorite: PropTypes.func.isRequired,
	toggleModal: PropTypes.func,
	setModalProps: PropTypes.func.isRequired,
};

CardsContainer.defaultProps = {
	toggleModal: () => {},
};
export default CardsContainer;

import React, { PureComponent } from 'react';
import styles from './CardItem.module.scss';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import star from '../../svg/star.svg';
import starFill from '../../svg/star_fill.svg';

class CardItem extends PureComponent {
	handleAddToCart = () => {
		const { toggleModal, setModalProps, article, name, url } = this.props;
		setModalProps({ article, name, url }); // Передаємо article та name до setModalProps
		toggleModal(true);
	};
	render() {
		const { name, prise, url, article, color, isFavorite, setCardFavorite } =
			this.props;

		return (
			<>
				<div className={styles.card}>
					<button
						type="button"
						className={styles.btn}
						onClick={() => {
							setCardFavorite(url);
						}}
					>
						<img
							className={styles.likeButton}
							src={!isFavorite ? star : starFill}
							alt="favorite"
						/>
					</button>

					<h4 className={styles.name}>{name}</h4>
					<img src={url} alt={name} className={styles.img} />
					<h5 className={styles.prise}>{prise}</h5>
					<h5 className={styles.article}>{article}</h5>
					<h5 className={styles.color}>{color}</h5>
					<div className={styles.btnContainer}>
						<Button
							text="Додати у корзину"
							backgroundColor="#ff4d4d"
							onClick={this.handleAddToCart}
						/>
					</div>
				</div>
			</>
		);
	}
}

CardItem.propTypes = {
	toggleModal: PropTypes.func,
	setModalProps: PropTypes.func.isRequired,
	name: PropTypes.string,
	price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	url: PropTypes.string,
	article: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
	color: PropTypes.string,
	isFavourite: PropTypes.bool,
	setCardFavorite: PropTypes.func.isRequired,
};

CardItem.defaultProps = {
	toggleModal: () => {},
	name: 'Лосіни',
	price: '0',
	url: './img/1.jpg',
	color: 'чорний',
	isFavourite: false,
};

export default CardItem;

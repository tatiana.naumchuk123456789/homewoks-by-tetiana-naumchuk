import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from './Modal.module.scss';

class Modal extends PureComponent {
	handleAddToCart = () => {
		const { modalProps, addToCarts, toggleModal } = this.props;
		addToCarts(modalProps); // Передаємо об'єкт modalProps в addToCarts
		toggleModal();
	};

	render() {
		const { header, closeButton, background, isModalOpen, toggleModal } =
			this.props;

		if (!isModalOpen) {
			return null;
		}

		return (
			<>
				<div className={styles.wrapper} style={{ background }}>
					<div className={styles.header}>
						<h2 className={styles.headerText}>{header}</h2>
						{closeButton && (
							<button
								className={styles.closeBtn}
								onClick={() => {
									toggleModal();
								}}
							>
								X
							</button>
						)}
					</div>
					<div className={styles.body}>
						<div className={styles.bodyBtn}>
							<button className={styles.button} onClick={this.handleAddToCart}>
								ТАК
							</button>
							<button
								className={styles.button}
								onClick={() => {
									toggleModal();
								}}
							>
								НІ
							</button>
						</div>
					</div>
				</div>
			</>
		);
	}
}

Modal.propTypes = {
	modalProps: PropTypes.object,
	addToCarts: PropTypes.func.isRequired,
	toggleModal: PropTypes.func,
	header: PropTypes.string,
	closeButton: PropTypes.bool,
	background: PropTypes.string,
	isModalOpen: PropTypes.bool,
};

Modal.defaultProps = {
	modalProps: {},
	toggleModal: () => {},
	header: 'Ви хочете додати цей товар у корзину?',
	closeButton: true,
	background: '#ff4d4d',
	isModalOpen: false,
};

export default Modal;

import React, { PureComponent } from 'react';
import CartItem from '../CartItem/CartItem';
import PropTypes from 'prop-types';
import styles from './CartContainer.module.scss';

class CartContainer extends PureComponent {
	render() {
		const { carts, incrementCartItem, dicrementCartItem } = this.props;
		return (
			<>
				<ul>
					{carts.map(({ url, article, count, name }) => {
						return (
							<CartItem
								url={url}
								article={article}
								count={count}
								name={name}
								incrementCartItem={incrementCartItem}
								dicrementCartItem={dicrementCartItem}
							/>
						);
					})}
				</ul>
			</>
		);
	}
}

CartContainer.propTypes = {
	carts: PropTypes.array.isRequired,
	incrementCartItem: PropTypes.func.isRequired,
	dicrementCartItem: PropTypes.func.isRequired,
};

export default CartContainer;

import React, { PureComponent } from 'react';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import styles from './CartItem.module.scss';

class CartItem extends PureComponent {
	render() {
		const { url, article, count, name, incrementCartItem, dicrementCartItem } =
			this.props;

		return (
			<>
				<div className={styles.cartItem}>
					<div className={styles.imgWrapper}>
						<img src={url} alt={name} className={styles.itemAvatar} />
					</div>

					<p className={styles.quantity}>{count}</p>

					<div className={styles.btnCart}>
						<Button
							className={styles.btn}
							text="+"
							onClick={() => {
								incrementCartItem(article);
							}}
							backgroundColor="#ff4d4d"
						/>
						<Button
							className={styles.btn}
							text=" – "
							onClick={() => {
								dicrementCartItem(article);
							}}
							backgroundColor="#ff4d4d"
						/>
					</div>
				</div>
			</>
		);
	}
}

CartItem.propTypes = {
	url: PropTypes.string,
	article: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
	count: PropTypes.number,
	name: PropTypes.string,
	incrementCartItem: PropTypes.func.isRequired,
	dicrementCartItem: PropTypes.func.isRequired,
};

CartItem.defaultProps = {
	url: './img/1.jpg',
	count: 0,
	name: 'Лосіни',
};

export default CartItem;

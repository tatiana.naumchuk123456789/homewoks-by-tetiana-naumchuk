import React, { Component } from 'react';
import Header from './components/Header/Header';
import CardsContainer from './components/CardsContainer/CardsContainer';
import CartContainer from './components/CartContainer/CartContainer';
import Modal from './components/Modal/Modal';
import styles from './App.module.scss';

class App extends Component {
	state = {
		data: [],
		cards: [],
		carts: [],
		favoriteCards: [],
		isModalOpen: false,
		isButton: true,
		modalProps: {},
	};

	async componentDidMount() {
		try {
			const data = await fetch('./data.json').then(res => res.json());
			const carts = localStorage.getItem('carts');
			if (carts) {
				this.setState({ carts: JSON.parse(carts) });
			}
			this.setState({ cards: data });
		} catch (error) {
			console.error();
		}
	}

	addToCarts = card => {
		this.setState(current => {
			const carts = [...current.carts];

			const index = carts.findIndex(el => el.article === card.article);

			if (index === -1) {
				carts.push({ ...card, count: 1 });
			} else {
				carts[index].count += 1;
			}

			localStorage.setItem('carts', JSON.stringify(carts));

			return { carts };
		});
	};

	incrementCartItem = article => {
		this.setState(current => {
			const carts = [...current.carts];

			const index = carts.findIndex(el => el.article === article);

			if (index !== -1) {
				carts[index].count += 1;
			}

			localStorage.setItem('carts', JSON.stringify(carts));

			return { carts };
		});
	};

	dicrementCartItem = article => {
		this.setState(current => {
			const carts = [...current.carts];

			const index = carts.findIndex(el => el.article === article);

			if (index !== -1) {
				carts[index].count -= 1;
			}

			if (carts[index].count < 1) {
				carts.splice(index, 1);
			}

			localStorage.setItem('carts', JSON.stringify(carts));

			return { carts };
		});
	};

	setCardFavorite = url => {
		this.setState(prev => {
			const favArray = [...prev.favoriteCards];
			let cards = [...prev.cards];
			const index = favArray.findIndex(item => item.url === url);
			if (index === -1) {
				favArray.push({ url, count: 1 });
			} else {
				favArray[index].count -= 1;
				if (favArray[index].count < 0) {
					favArray[index].count = 1;
				}
			}

			cards = cards.map(card => {
				if (card.url === url) {
					card.isFavorite = !card.isFavorite;
				}
				return card;
			});
			localStorage.setItem('favoriteCards', JSON.stringify(favArray));
			return { cards: cards, favoriteCards: favArray };
		});
	};

	openModal() {
		this.setState({ isModalOpen: true, isButton: false });
	}

	toggleModal = value => {
		this.setState({ isModalOpen: value });
	};

	setModalProps = value => {
		this.setState({ modalProps: value });
	};

	render() {
		const { cards, carts, favoriteCards, isModalOpen, modalProps } = this.state;

		return (
			<>
				<Header carts={carts} favoriteCards={favoriteCards} />
				<main>
					<section className={styles.leftContainer}>
						<CardsContainer
							cards={cards}
							setCardFavorite={this.setCardFavorite}
							isModalOpen={isModalOpen}
							toggleModal={this.toggleModal}
							setModalProps={this.setModalProps}
						/>
					</section>

					<section className={styles.rightContainer}>
						<h2>Корзина</h2>
						<CartContainer
							incrementCartItem={this.incrementCartItem}
							dicrementCartItem={this.dicrementCartItem}
							carts={carts}
						/>
					</section>
					<Modal
						isModalOpen={isModalOpen}
						toggleModal={this.toggleModal}
						background="#ff4d4d"
						header={`Ви хочете додати "${modalProps.name}" у корзину?`}
						closeButton={true}
						addToCarts={this.addToCarts}
						modalProps={modalProps}
					/>
				</main>
			</>
		);
	}
}

export default App;

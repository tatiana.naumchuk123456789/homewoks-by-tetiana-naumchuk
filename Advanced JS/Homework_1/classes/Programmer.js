class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this.lang = lang;
	}

	set lang(lang) {
		this._lang = lang;
	}
	get lang() {
		return this._lang;
	}

	set salary(salary) {
		this._salary = salary * 3;
	}
	get salary() {
		return this._salary;
	}
}

const prog1 = new Programmer('   YAna', 18, 25, 'ukrainian');
console.log(prog1);

const prog2 = new Programmer('VALENTINA', 20, 50, 'english');
console.log(prog2);

const prog3 = new Programmer('daVid', 27, 100, 'spanish');
console.log(prog3);

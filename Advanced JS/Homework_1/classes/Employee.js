class Employee {
	constructor(name, age, salary) {
		this.name = name;
		this.age = age;
		this.salary = salary;
	}
	set name(name) {
		this._name = name.trim().toLowerCase();
	}
	get name() {
		return this._name;
	}

	set age(age) {
		this._age = age;
	}
	get age() {
		return this._age;
	}

	set salary(salary) {
		this._salary = salary;
	}
	get salary() {
		return this._salary;
	}
}

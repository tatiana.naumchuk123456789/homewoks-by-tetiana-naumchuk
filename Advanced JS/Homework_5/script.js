const URL = 'https:ajax.test-danit.com/api/json/';
const root = document.querySelector('#root');
root.style.cssText = `
display: flex;
flex-wrap: wrap;
display: inline-flex;
gap: 5px;
justify-content: center;
max-width: 100%;
		`;

class Requests {
	constructor(url) {
		this.url = url;
	}

	getEntity(entity) {
		return fetch(this.url + `${entity}`).then(response => response.json());
	}

	deleteEntity(entity, id) {
		return fetch(this.url + entity + '/' + id, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
			},
		}).then(response => {
			if (!response.ok) {
				throw new Error('Something went wrong');
			}
			return response;
		});
	}
}

const request = new Requests(URL);

class Card {
	constructor(container) {
		this.container = document.createElement('div');
		this.container.style.cssText = `background: violet;
		border: 1px solid black;
		padding: 5px;
		max-width:50%;
		`;
	}

	renderUser(user) {
		const userData = document.createElement('div');

		const userName = document.createElement('h2');
		userName.textContent = `${user.username} ${user.name}`;

		const userEmail = document.createElement('p');
		userEmail.textContent = user.email;

		userData.append(userName, userEmail);
		this.container.append(userData);
	}
	renderPost(post) {
		const postData = document.createElement('div');
		const postTitle = document.createElement('h3');
		postTitle.textContent = post.title;

		const postText = document.createElement('p');
		postText.textContent = post.body;

		const btnDel = document.createElement('button');
		btnDel.textContent = 'X';

		btnDel.addEventListener('click', () => {
			request.deleteEntity('posts', post.id).then(data => {
				this.container.remove();
			});
		});

		postData.append(postTitle, postText);
		this.container.append(postData, btnDel);
	}
}

Promise.all([request.getEntity('users'), request.getEntity('posts')])
	.then(([users, posts]) => {
		users.forEach(user => {
			const postFilter = posts.filter(elem => elem.userId === user.id);
			postFilter.forEach(item => {
				const card = new Card();

				card.renderUser(user);
				card.renderPost(item);
				root.append(card.container);
			});
		});
	})
	.catch(error => {
		console.error(error);
	});

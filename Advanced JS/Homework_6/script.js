const userIp = 'https://api.ipify.org/?format=json';
const userAdress = 'http://ip-api.com/';
const root = document.querySelector('#root');

class Request {
	constructor(url) {
		this.url = url;
	}

	async getIp() {
		const ipResponse = await fetch(this.url);
		const ipData = await ipResponse.json();
		const ipAdress = ipData.ip;

		const adressResponse = await fetch(`${userAdress}json/${ipAdress}`);
		const adressData = await adressResponse.json();
		return adressData;
	}
}

class FindIp {
	createBtn(text) {
		const btnIp = document.createElement('button');
		btnIp.textContent = text;
		btnIp.style.cssText = `
border-radius: 5px;
background: aqua;
padding: 10px 25px;
`;
		root.append(btnIp);

		btnIp.addEventListener('click', async () => {
			const request = new Request(userIp);
			const data = await request.getIp();
			const infoUser = document.createElement('div');
			infoUser.innerText = `
			Континент: ${data.timezone}
			Країна: ${data.country}
			Регіон: ${data.region}
			Місто: ${data.city}
			Район: ${data.regionName}
			`;
			root.append(infoUser);
		});
	}
}

const findIp = new FindIp();
findIp.createBtn('Знайти по IP');

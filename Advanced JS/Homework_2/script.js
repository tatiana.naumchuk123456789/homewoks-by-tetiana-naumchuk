const books = [
	{
		author: 'Люсі Фолі',
		name: 'Список запрошених',
		price: 70,
	},
	{
		author: 'Сюзанна Кларк',
		name: 'Джонатан Стрейндж і м-р Норрелл',
	},
	{
		name: 'Дизайн. Книга для недизайнерів.',
		price: 70,
	},
	{
		author: 'Алан Мур',
		name: 'Неономікон',
		price: 70,
	},
	{
		author: 'Террі Пратчетт',
		name: 'Рухомі картинки',
		price: 40,
	},
	{
		author: 'Анґус Гайленд',
		name: 'Коти в мистецтві',
	},
];

let div = document.createElement('div');
div.id = 'root';
document.body.append(div);

function booksList(arr) {
	let ul = document.createElement('ul');
	arr.forEach(elems => {
		let li = document.createElement('li');
		li.textContent = `author: ${elems.author}; name: ${elems.name}; price: ${elems.price}`;

		try {
			if (elems.author && elems.name && elems.price) {
				ul.append(li);
			} else {
				throw new Error('Недостатньо даних');
			}
		} catch (e) {
			if (elems.author === undefined) {
				console.error(`У книзі '${elems.name}' не вистачає властивості author`);
			}
			if (elems.name === undefined) {
				console.error(`У книзі '${elems.name}' не вистачає властивості name`);
			}
			if (elems.price === undefined) {
				console.error(`У книзі '${elems.name}' не вистачає властивості price`);
			}
		}
	});
	return ul;
}

let booksUl = booksList(books);
document.getElementById('root').append(booksUl);

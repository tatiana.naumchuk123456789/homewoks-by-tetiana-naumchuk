const URL = 'https://ajax.test-danit.com/api/swapi/';
const root = document.querySelector('#root');

class Requests {
	constructor(url) {
		this.url = url;
	}

	getEntity(entity) {
		return fetch(this.url + `${entity}`).then(response => {
			return response.json();
		});
	}
}

class StarWars {
	constructor(root) {
		this.root = root;
	}

	renderEntity(film) {
		const infoEntity = document.createElement('div');

		const entityContainer = document.createElement('div');
		const title = document.createElement('h3');
		title.textContent = `Film name: "${film.name}"`;
		title.style.color = 'blue';

		const id = document.createElement('p');
		id.textContent = `Episode: ${film.episodeId}`;
		id.style.fontWeight = 'bold';

		const description = document.createElement('p');
		description.textContent = `Description: ${film.openingCrawl}`;

		const characters = document.createElement('ul');
		const charTitle = document.createElement('h4');
		charTitle.textContent = 'Characters:';

		film.characters.forEach(characterURL => {
			const character = document.createElement('li');
			fetch(characterURL)
				.then(response => {
					return response.json();
				})
				.then(characterData => {
					character.textContent = characterData.name;
				});
			characters.append(character);
		});

		entityContainer.append(title, charTitle, characters, id, description);
		infoEntity.append(entityContainer);
		this.root.append(infoEntity);
	}
}

const request = new Requests(URL);
const starWars = new StarWars(root);

request
	.getEntity('films')
	.then(data => {
		data.forEach(film => {
			starWars.renderEntity(film);
		});
	})
	.catch(error => {
		console.error(error);
	});
